#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int jsh_cd(char **args);
int jsh_help(char **args);
int jsh_exit(char **args);

char *builtin_str[] = {
  "cd",
  "help",
  "exit"
};

int (*builtin_func[]) (char **) = {
  &jsh_cd,
  &jsh_help,
  &jsh_exit
};

int jsh_num_builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

int jsh_cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, "jsh: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("jsh");
    }
  }
  return 1;
}

int jsh_help(char **args)
{
  int i;
  printf("Jae's shell.");

  for (i = 0; i < jsh_num_builtins(); i++) {
    printf("  %s\n", builtin_str[i]);
  }

  printf("Use the man command for information on other programs.\n");
  return 1;
}

int jsh_exit(char **args)
{
  return 0;
}

int jsh_launch(char **args)
{
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("jsh");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    // Error forking
    perror("jsh");
  } else {
    // Parent process
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

int jsh_execute(char **args)
{
  int i;

  if (args[0] == NULL) {
    return 1;
  }

  for (i = 0; i < jsh_num_builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }

  return jsh_launch(args);
}

#define jsh_RL_BUFSIZE 1024
char *jsh_read_line(void)
{
  int bufsize = jsh_RL_BUFSIZE;
  int position = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "jsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    c = getchar();

    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    if (position >= bufsize) {
      bufsize += jsh_RL_BUFSIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "jsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

#define jsh_TOK_BUFSIZE 64
#define jsh_TOK_DELIM " \t\r\n\a"
char **jsh_split_line(char *line)
{
  int bufsize = jsh_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token, **tokens_backup;

  if (!tokens) {
    fprintf(stderr, "jsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, jsh_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += jsh_TOK_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
		free(tokens_backup);
        fprintf(stderr, "jsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, jsh_TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens;
}

void jsh_loop(void)
{
  char *line;
  char **args;
  int status;

  do {
    printf("~[JSH]> ");
    line = jsh_read_line();
    args = jsh_split_line(line);
    status = jsh_execute(args);

    free(line);
    free(args);
  } while (status);
}

int main(int argc, char **argv)
{
  jsh_loop();
  return EXIT_SUCCESS;
}

